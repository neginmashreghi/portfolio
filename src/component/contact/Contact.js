import React, { Component } from 'react';
import ContactImage from '../../image/contact-me-white-lable.png'
import ContactModalBackground from '../../image/contact-modal.png'
import closeModal from '../../image/contatc-close-modal.png'
import { Modal } from 'antd';
import './contact.css'

class Contact extends Component {
    constructor (props) {
        super (props);
        this.state = {
           visible: false 
        }
     }
    
     showModal = () => {
        console.log("showModal")
        this.setState({
           visible: true,
        });
     };
  
     closeModal = e => {
        this.setState({
          visible: false,
        });
    
      };
    render() {
        return (
            <div className="contact-wrapper">
                <img className="contact-image-button" src={ContactImage} alt="contact-image-button" onClick={this.showModal}/>
                <Modal 
                    className="contact-modal" 
                    width="auto"
                    centered = {true}
                    maskStyle={{background: "rgba(0, 0, 0, 0.4)"}} 
                    footer={null}  
                    visible={this.state.visible}
                    onCancel={this.closeModal} 
                    >
                        <div className="contact-modal-body">  
                            <img className="contact-modal-background" src={ContactModalBackground} alt="my image" />
                            <img className="contact-close-modal" src={closeModal} alt="my image" onClick={this.closeModal} />
                            <div className='contact-me-content'>
                                
                                <p>My Contact</p>
                               
                                <p>Email:</p>
                            
                                <p>negin_mashreghi@yahoo.com</p>
                                
                                <p>Linkedin:</p>
                                
                                <p><a href="https://www.linkedin.com/in/NeginMashreghi" target="_blank">Linkedin.com/in/NeginMashreghi</a></p>
                               
                                <p>Bitbucket:</p>
                                
                                <p><a href="https://bitbucket.org/neginmashreghi" target="_blank">bitbucket.org/neginmashreghi</a></p>
                               
                                
                            </div>
                        
                        </div>
                </Modal>
            </div>
        );
    }
}

export default Contact;