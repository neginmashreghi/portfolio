import React, { Component } from 'react';
import MyImage from '../../image/me-on-table.png'
import backgroundModal from '../../image/Me-background-Modal.png'
import FullMeImage from '../../image/Me-Modal.png'
import ClosingButtom from '../../image/Me-closing-Modal.png'
import { Modal } from 'antd';
import './aboutMe.css'


class AboutMe extends Component {
    
    constructor (props) {
        super (props);
        this.state = {
           visible: false 
        }
     }
    
     showModal = () => {
        console.log("showModal")
        this.setState({
           visible: true,
        });
     };
  
     closeModal = e => {
        this.setState({
          visible: false,
        });
    
      };
   
    render() {
        return (
            <div className="aboutme-wrapper">
                <img className="aboutme-image-button" src={MyImage} alt="me-image-button" onClick={this.showModal} />
                <Modal 
                className="aboutme-modal" 
                width="auto"
                centered = {true}
                maskStyle={{background: "rgba(0, 0, 0, 0.4)"}} 
                footer={null}  
                visible={this.state.visible}
                onCancel={this.closeModal} 
                 >
                    <div className="aboutme-modal-content" >
                    <img className="background-modal-me" src={backgroundModal} alt="my image"  />
                    <img className="closing-buttom" src={ClosingButtom} alt="my image"  onClick={this.closeModal} />
                    <img className="full-me-image" src={FullMeImage} alt="my image"  /> 
                   
                    <div className="aboutme-section">
                       <h2>Hey I’m Negin </h2>
                       <p >
                       
                     I am a Software Developer with a graphic design background.  
My interest in coding began when I come across a YouTube video that showed an email marketing template with an action button being designed in Illustrator, after which that same design was created from scratch using HTML and CSS and displayed in a browser. It was the moment I saw that the button was clickable that I realized a graphic designer creates art and a Software Developer brings it to life.
</p>
                       </div> 
                     </div>
                </Modal>
           </div>
        );
    }
}

export default AboutMe;