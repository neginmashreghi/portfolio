import React, { Component } from 'react';
import ResumeImage from '../../image/resume-white-lable.png'
/* import BackGroundImageModal from '../../image/resume-view.png' */
import BackGroundImageModal from '../../image/resume_page.jpg'
import resumePdf from '../../pdf/portfolio.pdf'
import closeButton from '../../image/resume-close-button.png'
import downloadButton from '../../image/resume-downlaod-button.png'
import linkedin from '../../image/resume-linkedin.png'
import bitbucket from '../../image/resume-bitbucket-button.png'


import { Modal } from 'antd';
import './resume.css'

class Resume extends Component {
    constructor (props) {
        super (props);
        this.state = {
           visible: false 
        }
     }
    
     showModal = () => {
        console.log("showModal")
        this.setState({
           visible: true,
        });
     };
  
     closeModal = e => {
        this.setState({
          visible: false,
        });
    
      };
      

    render() {
        return (
            <div className="resume-wrapper">
            <img  className="resume-image-buttom" src={ResumeImage} alt="resume-image-buttom" onClick={this.showModal} />
            <Modal 
                className="resume-modal" 
                width="auto"
                centered = {true}
                maskStyle={{background: "rgba(0, 0, 0, 0.4)"}} 
                footer={null}  
                visible={this.state.visible}
                onCancel={this.closeModal} 
                 >

                    <div className="resume-modal-content">
                        <div className="img_button_continer" style={{ }}>
                       
                          <div  className="button_continer" style={{ } }>
                             
                              <a href={resumePdf} download >
                              <img className="resume-btn" src={downloadButton} alt="my image"   />
                              </a>
                              <img  className="resume-btn"  src={closeButton} alt="my image" onClick={this.closeModal} />
                              <a href="https://www.linkedin.com/in/NeginMashreghi" target="_blank" >
                              <img className="resume-btn" src={linkedin} alt="my image"   />
                              </a>
                              <a href="https://www.linkedin.com/in/neginmashreghi/" target="_blank" >
                              <img className="resume-btn" src={bitbucket} alt="my image"   />
                              </a> 
                          </div> 
                          <div> 
                           <img  className="background-image-modal" src={BackGroundImageModal} alt="background-image-modal" onClick={this.showModal} /> 
                           </div> 
                        </div> 
                           
                    </div>
                </Modal>
            </div>
        );
    }
}

export default Resume;