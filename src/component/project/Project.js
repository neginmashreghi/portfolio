import React, { Component } from 'react';
import ComputerImage from '../../image/computer-white-lable.png'
import Closing from '../../image/close-project.png'
import { Modal } from 'antd';
import './project.css'
class Project extends Component {
    constructor (props) {
        super (props);
        this.state = {
           visible: false 
        }
     }
    
     showModal = () => {
        console.log("showModal")
        this.setState({
           visible: true,
        });
     };
  
     closeModal = e => {
        this.setState({
          visible: false,
        });
    
      };
    render() {
        return (
            <div className="computer-wrapper">
                <img  className="computer-image-button" src={ComputerImage} alt="computer-image-button" onClick={this.showModal} />
                <Modal 
                className="computer-modal" 
                width="auto"
                centered = {true}
                maskStyle={{background: "rgba(0, 0, 0, 0.4)"}} 
                footer={null}  
                visible={this.state.visible}
                onCancel={this.closeModal} 
                >
                    
                    <div className="computer-screen">
                       
                        <div className="terminal-screen">
                        <img  className="closing-myproject-modal" src={Closing} alt="Closing" onClick={this.closeModal} />
                        </div>
                            <div className="terminal">
                            <p>Last login: Thu Aug 22 2019</p>
                            <div className="second-line">
                                <p>Negins-Computer:~ neginmashreghi$</p>
                                <p class="typing-1">cd MyProject</p>
                            </div>
                            <div className="third-line">
                                <p class="show-p-tag">Negins-Computer:MyProject neginmashreghi$</p>
                                <p class="typing-2"> ls</p>
                            </div>
                            <div class="show-project-list">
                            
                              
                                <a href="https://github.com/NeginMashreghi/CaptoneProject-Arduino-AWS" target="_blank">
                                    <p>  &nbsp; HDL-Sensor-Project</p>
                                </a>
                                
                                <a href="https://bitbucket.org/neginmashreghi/sms-authentication/src/master/" target="_blank">
                                    <p>  &nbsp; SMS-Authentication-App</p>
                                </a>
                                <a href="https://bitbucket.org/neginmashreghi/sms-authentication-test/src/master/" target="_blank">
                                    <p>  &nbsp; SMS-Authentication-Test</p>
                                </a>
                                <a href="https://bitbucket.org/neginmashreghi/email-verification/src/master/" target="_blank">
                                    <p>  &nbsp; Email-Comformation-App</p>
                                </a>
                                <a href="https://bitbucket.org/neginmashreghi/email-verification-test/src/master/" target="_blank">
                                    <p>  &nbsp; Email-Comformation-Test</p>
                                </a>
                                <a href="https://bitbucket.org/neginmashreghi/addnumber/src/master/" target="_blank">
                                    <p>  &nbsp; Auto-Deploy-Test-Bitbucket-Pipeline</p>
                                </a>
                                <a href="https://bitbucket.org/neginmashreghi/datecalculator_frontend/src/master/" target="_blank">
                                    <p>  &nbsp; Date-Calculator</p>
                                </a>
                              
                                
                            
                            </div>  
                        </div> 
                        
                    </div>
                       
                  
                </Modal>
            </div>
        );
    }
}

export default Project;