import React, { Component } from 'react';
import My from './component/aboutMe/AboutMe'
import MyResume from './component/resume/Resume'
import MyContact from './component/contact/Contact'
import MyProject from './component/project/Project'
import './mainCSS.css'


class MyTable extends Component {
  

    render() {
        return (
            <div className="main-content">
             
           <My />
         <MyProject
                myProject={this.myProject}
            />
                <MyResume
                myResume={this.myResume}
                />
                <MyContact
                myContact={this.myContact}
                />
                
                    
            </div>
        );
    }
}

export default MyTable;